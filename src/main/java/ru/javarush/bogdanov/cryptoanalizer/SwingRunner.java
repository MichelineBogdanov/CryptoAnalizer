package ru.javarush.bogdanov.cryptoanalizer;

import ru.javarush.bogdanov.cryptoanalizer.view.SwingForm;

public class SwingRunner {

    public static void main(String[] args) {
        SwingForm jFrame = new SwingForm();
        //jFrame.setContentPane(jFrame.getMainPanel());
        //jFrame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        //jFrame.pack();
        jFrame.setVisible(true);
    }

}
